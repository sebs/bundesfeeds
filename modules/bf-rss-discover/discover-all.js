var mdb = require('bf-mdb-bundestag')().bundesfeeds.all();
var async = require('async');

// create a queue object with concurrency 2

var request = require('hyperquest-timeout')
var discover = require('feed-discover')


var results =[]

var q = async.queue(function(task, callback) {

  if (!task.id) {
    callback();
    return;
  }

  request(task.id)
    .pipe(discover(task.id))
    .on('end', (end)=>{
      console.log();
      callback();
    });
}, 1);

// assign a callback
q.drain = function() {
  console.log('done', results.length);
};

// add some items to the queue
q.push(mdb, function(err) {
  console.log('feeds:', results.length);
});
