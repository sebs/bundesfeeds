var bundestag = require('./lib/bundestag');
var bundesfeeds = require('./lib/bundesfeeds');
module.exports = function() {

  return {
    bundestag: bundestag(),
    bundesfeeds: bundesfeeds()
  }
}
