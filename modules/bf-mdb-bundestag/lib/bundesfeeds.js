var elasticlunr = require('elasticlunr');
var mdbs = require('../bundesfeeds-mdb.json')

var index = elasticlunr(function () {
    this.addField('name');
    this.addField('rss');
});

module.exports = function() {

  var docs = mdbs.map((person)=>{
    var name = person.nachname +  ', ' + person.vorname
    var doc = {
        id: person.websiteurl,
        rss: person.rsswebsiteurl || '',
        name: name
    }
    index.addDoc(doc);
    return doc;
  });

  return {
    all() {
      return docs;
    },
    getDoc(searchResult) {
      return index.documentStore.getDoc(searchResult.ref)
    },
    search(q) {
      var searchOpt = {
        fields: {
          name: {boost: 2},
          rss: {boost: 1},
        },
        bool: "OR",
        expand: true
      }
      return index.search(q, searchOpt);
    }
  }
}
