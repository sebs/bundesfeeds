var elasticlunr = require('elasticlunr');
var mdbs = require('../mdb.json').mdbUebersicht.mdbs.mdb;

var index = elasticlunr(function () {
    this.addField('name')
    this.addField('fraktion')
});

module.exports = function() {

  var docs = mdbs.map((person)=>{
    var fraktion = person.fraktion;
    var name = person.mdbName['$t'];
    var id = person.mdbInfoXMLURL;

    var doc = {
        id,
        name,
        fraktion
    };
    index.addDoc(doc);
    return doc;
  });

  return {
    all() {
      return docs;
    },
    getDoc(searchResult) {
      return index.documentStore.getDoc(searchResult.ref)
    },
    search(q) {
      var searchOpt = {
        fields: {
          name: {boost: 2},
          fraktion: {boost: 1}
        }
      }
      return index.search(q, searchOpt);
    }
  }
}
