var cheerio = require('cheerio');

module.exports = (html)=>{
  $ = cheerio.load(html);
  var feeds = [];

  var extract = function() {
    var feed = $(this).attr('href');
    feeds.push(feed);
  };

  // Legit
  $('link[type*=rss]').each(extract)
  $('link[type*=atom]').each(extract)
  return feeds;
};
